#include "perm.h"
void Perm::perm(int *choices, int chosen, int n, Util *util) {
  if(chosen >= n) {
    util->array_print(choices, n);
  } else {
    for(int i = chosen; i < n; i++) {
      util->array_swap(choices, i, chosen);
      perm(choices, chosen + 1, n, util);
      util->array_swap(choices, i, chosen);
    }
  }
}

