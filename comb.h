#ifndef COMB_H
#define COMB_H
#include "util.h"
class Comb {
  public:
    void comb(int *choices, int chosen, int k, int *from, int current, int n, Util *util);
};
#endif
