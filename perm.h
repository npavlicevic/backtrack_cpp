#ifndef PERM_H
#define PERM_H
#include "util.h"
class Perm {
  public:
    void perm(int *choices, int chosen, int n, Util *util);
};
#endif
