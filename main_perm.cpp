#include "perm.h"

int main() {
  Util *util = new Util();
  Perm *perm = new Perm();
  int n;
  fscanf(stdin, "%d", &n);
  int *choices = new int[n];
  util->array_init(choices, n);
  util->array_print(choices, n);
  perm->perm(choices, 0, n, util);
  delete choices;
  delete perm;
  delete util;
  return 0;
}
