#include "nqueens.h"

int main() {
  Nqueens *n = new Nqueens();
  int *cols = new int[8];
  n->nqueens(cols, 0, 8);
  delete cols;
  delete n;
  return 0;
}
