#include "comb.h"
void Comb::comb(int *choices, int chosen, int k, int *from, int current, int n, Util *util) {
  if(chosen >= k) {
    util->array_print(choices, chosen);
  } else {
    for(int i = current; i < n; i++) {
      choices[chosen] = from[i];
      comb(choices, chosen + 1, k, from, i + 1, n, util);
    }
  }
}
