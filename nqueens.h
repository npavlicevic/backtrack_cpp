#ifndef NQUEENS_H
#define NQUEENS_H
#include <stdlib.h>
#include <stdio.h>
class Nqueens {
  public:
    int safe(int *cols, int col);
    void nqueens(int *cols, int col, int colsn);
    void print(int *cols, int colsn);
};
#endif
