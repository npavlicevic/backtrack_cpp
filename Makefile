#
# Makefile 
# backtrack
#

CC=g++
AR=ar
CFLAGS=-g -Wall
OBJ_FLAGS=-c -static
AR_FLAGS=rcs
LIBS=-lm

FILES=nqueens.cpp main.cpp
FILES_PERM=util.cpp perm.cpp main_perm.cpp
FILES_COMB=util.cpp comb.cpp main_comb.cpp
OBJ_CPP_ARRAY_UTIL=util.cpp
LIB_CPP_ARRAY_UTIL=cpp_array_util.o
CLEAN=main main_perm main_comb objarrayutilcpp

all: main main_perm main_comb objcpparrayutil libcpparrayutil

main: ${FILES}
	${CC} ${CFLAGS} $^ -o main ${LIBS}

main_perm: ${FILES_PERM}
	${CC} ${CFLAGS} $^ -o main_perm ${LIBS}

main_comb: ${FILES_COMB}
	${CC} ${CFLAGS} $^ -o main_comb ${LIBS}

objcpparrayutil: ${OBJ_CPP_ARRAY_UTIL}
	${CC} ${OBJ_FLAGS} $^ -o cpp_array_util.o ${LIBS}

libcpparrayutil: ${LIB_CPP_ARRAY_UTIL}
	${AR} ${AR_FLAGS} libcpparrayutil.a $^

clean: ${CLEAN}
	rm $^
