#include "nqueens.h"
int Nqueens::safe(int *cols, int col) {
  for(int i = 0; i < col; i++) {
    if(cols[col] == cols[i]) return 0;
    if(abs(cols[col] - cols[i]) == col - i) return 0;
  }
  return 1;
}
void Nqueens::nqueens(int *cols, int col, int colsn) {
  if(col >= colsn) {
    print(cols, colsn);
  } else {
    for(int i = 0; i < colsn; i++) {
      cols[col] = i;
      if(safe(cols, col)) {
        nqueens(cols, col + 1, colsn);
      }
    }
  }
}
void Nqueens::print(int *cols, int colsn) {
  for(int i = 0; i < colsn; i++) {
    for(int j = 0; j < colsn; j++) {
      if(cols[j] == i) printf("Q ");
      else printf("+ ");
    }
    printf("\n");
  }
  printf("\n");
}
