#include "comb.h"

int main() {
  Util *util = new Util();
  Comb *comb = new Comb();
  int n, k;
  fscanf(stdin, "%d", &n);
  int *from = new int[n];
  util->array_init(from, n);
  util->array_print(from, n);
  fscanf(stdin, "%d", &k);
  int *choices = new int[k];
  comb->comb(choices, 0, k, from, 0, n, util);
  delete choices;
  delete from;
  delete comb;
  delete util;
  return 0;
}
