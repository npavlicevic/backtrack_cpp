#include "util.h"
void Util::array_init(int *arr, int n) {
  for(int i = 0; i < n; i++) {
    arr[i] = rand() % 128;
  }
}
void Util::array_copy(int *dest, int *src, int from, int to) {
  int i = from;
  for(; i < to; i++) {
    dest[i] = src[i];
  }
}
void Util::array_swap(int *arr, int from, int to) {
  int temp = arr[to];
  arr[to] = arr[from];
  arr[from] = temp;
}
void Util::array_print(int *arr, int n) {
  for(int i = 0; i < n; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}
